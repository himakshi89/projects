package com.pwc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Calendar {

	static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

	public static void main(String arp[]) throws ParseException {
		List<Record> recordList = new ArrayList<Record>();
		Record record = new Record();
		record.setApplicationName("PwC");
		record.setBeginDate(sdf.parse("02-11-2020"));
		record.setEndDate(sdf.parse("10-11-2020"));
		recordList.add(record);
		record = new Record();
		record.setApplicationName("CTS");
		record.setBeginDate(sdf.parse("01-10-2020"));
		record.setEndDate(sdf.parse("30-10-2020"));
		recordList.add(record);
		record = new Record();
		record.setApplicationName("IBM");
		record.setBeginDate(sdf.parse("08-11-2020"));
		record.setEndDate(sdf.parse("12-11-2020"));
		recordList.add(record);
		record = new Record();
		record.setApplicationName("BBC");
		record.setBeginDate(sdf.parse("02-11-2021"));
		record.setEndDate(sdf.parse("30-11-2021"));
		recordList.add(record);
		record = new Record();
		record.setApplicationName("CNN");
		record.setBeginDate(sdf.parse("01-10-2020"));
		record.setEndDate(sdf.parse("30-11-2020"));
		recordList.add(record);
		System.out.println("Record size:" + recordList.size());
		while (true) {
			System.out.println("Input a date in dd-MM-yyyy");
			Scanner scanObj = new Scanner(System.in);
			String input = scanObj.nextLine();
			Date today;
			try {
				today = sdf.parse(input);
				System.out.println("________________");
				System.out.println("Today:" + sdf.format(today));
				for (Record r : recordList) {
					if (r.getBeginDate().compareTo(today) > 0 && r.getEndDate().compareTo(today) > 0) {
						System.out.println(r.getApplicationName() + " : " + "Future:" + sdf.format(r.getBeginDate())
								+ " : " + sdf.format(r.getEndDate()));
					} else if (r.getBeginDate().compareTo(today) * today.compareTo(r.getEndDate()) < 0) {
						System.out.println(r.getApplicationName() + " : " + "Past:" + sdf.format(r.getBeginDate())
								+ " : " + sdf.format(r.getEndDate()));
					} else if (r.getBeginDate().compareTo(today) == 0 || r.getEndDate().compareTo(today) == 0
							|| (r.getBeginDate().compareTo(today) * today.compareTo(r.getEndDate()) > 0)) {
						System.out.println(r.getApplicationName() + " : " + "Current:" + sdf.format(r.getBeginDate())
								+ " : " + sdf.format(r.getEndDate()));
					}
				}
			} catch (ParseException e) {
				System.out.println("Program exited");
				System.exit(0);
			}
		}
	}
}
