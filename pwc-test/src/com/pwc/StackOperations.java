package com.pwc;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Stack is a data structure which follows LIFO architecture. Please write a
 * stack of Integer witch the following operation:
 * 
 * 1. add an element 2. delete and element 3. traverse through it to print the
 * elements
 * @author himakshi mangal
 */
public class StackOperations {

	static List<Integer> stackElement = new LinkedList<Integer>();

	public static void main(String ar[]) throws IOException {
		while (true) {
			System.out.println("---------------------------");
			System.out.println("Please choose an option");
			System.out.println("1. Add an element");
			System.out.println("2. Delete an element");
			System.out.println("3. Traverse the stack");
			System.out.println("Press any key to exit");
			System.out.println("---------------------------");
			Scanner system = new Scanner(System.in);
			String option = system.next();
			switch (option) {
			case "1":
				System.out.println("Enter an element:");
				Scanner readElement = new Scanner(System.in);
				String element = system.next();
				addElement(Integer.valueOf(element));
				break;
			case "2":
				if (stackElement.size() == 0) {
					System.out.println("--- No element present to be removed ---");
				} else {
					deleteElement(stackElement.size() - 1);
				}
				break;
			case "3":
				traverse();
				break;
			default:
				System.out.println("You exited the program");
				System.exit(1);
				
				break;
			}
		}
	}

	public static void addElement(Integer element) {
		stackElement.add(element);
		System.out.println("-- Element added to the list:" + element);
		System.out.println("-- Size of the stack:" + stackElement.size());
	}

	public static void deleteElement(int index) {
		stackElement.remove(index);
		System.out.println("--- Element removed from the list. Size of the stack:" + stackElement.size() + " ---");

	}

	public static void traverse() {
		System.out.println("---- Traversing the stack ---");
		if (stackElement.size() == 0) {
			System.out.println("Stack is empty. Please add elements to print");
		} else {
			List<Integer> traverseList = new LinkedList<Integer>();
			traverseList.addAll(stackElement);
			Collections.reverse(traverseList);
			for (Integer i : traverseList) {
				System.out.println(i);
			}
		}
	}

}
